
import React, { Component } from 'react';
import './App.css';
import Contenedor from './components/Container'

// import Visualization from './Visualization'
// import Custom from './Custom'
// import Cone from './Cone'
// import Stl from './Stl'
// import Stlreader from './Stlreader'
// import View from './View'

import vtkConeSource from 'vtk.js/Sources/Filters/Sources/ConeSource';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileSelected: null,
      extraProp: 1,
      cone: null,
      deleteCone: null
    }
  }

  _handleChange = (selectorFiles) => {
    // console.log(selectorFiles);
    // console.log(selectorFiles.target.files);
    // const files = selectorFiles.target.files || selectorFiles.dataTransfer.files;
    // const fileReader = new FileReader();
    // fileReader.readAsArrayBuffer(files[0]);
    // fileReader.onload = (e) => {
    //   this.setState({
    //     fileSelected: fileReader.result,
    //     extraProp: this.state.extraProp + 1
    //   })
    // };
  }

_handleChangeChk = ()=> {
  if (!this.state.actor){
    const coneSource = vtkConeSource.newInstance();
    const actor = vtkActor.newInstance();
    const mapper = vtkMapper.newInstance();
    actor.setMapper(mapper);
    mapper.setInputConnection(coneSource.getOutputPort());
    this.setState({
      cone:actor,
      deleteCone: null
    })
  } else {
    this.setState({
      cone:null,
      deleteCone: this.state.actor
    })
  }
}


  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <input type="file" onChange={this._handleChange} class="file" /> */}
          {/* <input type="checkbox" onChange={this._handleChangeChk} /> */}
          {/* <Stlreader></Stlreader> */}
          {/* <Stl stlFile={this.state.fileSelected} extraProp={this.state.extraProp}></Stl> */}
          {/* <View actor={this.state.cone} deleteActor={this.state.deleteCone}></View> */}
          <Contenedor></Contenedor>
        </header>
      </div>
    );
  }
}

export default App;
