import React from 'react'

import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkSphereSource from 'vtk.js/Sources/Filters/Sources/SphereSource';
import vtkConeSource from 'vtk.js/Sources/Filters/Sources/ConeSource';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkFullScreenRenderWindow from 'vtk.js/Sources/Rendering/Misc/FullScreenRenderWindow';
import vtkTubeFilter from 'vtk.js/Sources/Filters/General/TubeFilter';
import vtkPoints from 'vtk.js/Sources/Common/Core/Points';
import vtkPolyData from 'vtk.js/Sources/Common/DataModel/PolyData';
import { VtkDataTypes } from 'vtk.js/Sources/Common/Core/DataArray/Constants';



class Custom extends React.Component {

    constructor(props) {
        super(props)
        this.fullScreenRenderer = null;
        this.container = React.createRef();
        this.pipeline = null;
    }

    createPipeline(resolution = 20) {
        const sphereSource = vtkSphereSource.newInstance({ radius: 1.5, thetaResolution: resolution, phiResolution: resolution });
        const ConeSource = vtkConeSource.newInstance();

        // const mapper = vtkMapper.newInstance();
        // mapper.setInputConnection(coneSource.getOutputPort());
        // const actor = vtkActor.newInstance();
        // actor.setMapper(mapper);

        // const atomColorR = 0.380
        // const atomColorG = 0.855
        // const atomColorB = 0.984

        const sphereMapper = vtkMapper.newInstance();
        sphereMapper.setInputConnection(ConeSource.getOutputPort());

        const sphereActor = vtkActor.newInstance();
        sphereActor.setMapper(sphereMapper);
        // sphereActor.getProperty().setColor(atomColorR, atomColorG, atomColorB)



/*        // const polyDataYZ = vtkPolyData.newInstance();
        // const pointsYZ = vtkPoints.newInstance({ dataType: pointType });
        // pointsYZ.setNumberOfPoints(numberOfSegments + 1);
        // const pointDataYZ = new Float32Array(3 * (numberOfSegments + 1));
        // const vertsYZ = new Uint32Array(2 * (numberOfSegments + 1));
        // const linesYZ = new Uint32Array(numberOfSegments + 2);
        // linesYZ[0] = numberOfSegments + 1;

        // for (let i = 0; i < numberOfSegments + 1; i++) {
        //     for (let j = 0; j < 3; j++) {
        //         const angle = i / (numberOfSegments - 1) * 2.0 * Math.PI;
        //         pointDataYZ[3 * i + 0] = 0.0;
        //         pointDataYZ[3 * i + 1] = tubeRadius * Math.cos(angle);
        //         pointDataYZ[3 * i + 2] = tubeRadius * Math.sin(angle);
        //     }
        //     vertsYZ[i] = 1;
        //     vertsYZ[i + 1] = i;
        //     linesYZ[i + 1] = i;
        // }
        // pointsYZ.setData(pointDataYZ);
        // polyDataYZ.setPoints(pointsYZ);
        // polyDataYZ.getVerts().setData(vertsYZ);
        // polyDataYZ.getLines().setData(linesYZ);
        */

        // const tubeFilterYZ = vtkTubeFilter.newInstance();
        // tubeFilterYZ.setCapping(false);
        // tubeFilterYZ.setNumberOfSides(resolution);
        // tubeFilterYZ.setRadius(0.5);

        // tubeFilterYZ.setInputData(polyDataYZ);


        // const tubeMapperYZ = vtkMapper.newInstance();
        // tubeMapperYZ.setInputConnection(tubeFilterYZ.getOutputPort());

        // const tubeActorYZ = vtkActor.newInstance();
        // tubeActorYZ.setMapper(tubeMapperYZ)
        
        // tubeActorYZ.getProperty().setColor(atomColorR, atomColorG, atomColorB)

        return { sphereMapper, sphereActor};
    }

    updatePipeline() {
        const renderer = this.fullScreenRenderer.getRenderer();
        const renderWindow = this.fullScreenRenderer.getRenderWindow();

        if (this.pipeline) {
            renderer.removeActor(this.pipeline.actor);
            this.pipeline = null;
        }

        const resolution = this.props.resolution || 40
        this.pipeline = this.createPipeline(resolution);
        const pipeline = this.pipeline
        renderer.addActor(pipeline.sphereActor);
        renderer.addActor(pipeline.tubeActorXY);
        renderer.addActor(pipeline.tubeActorXZ);
        renderer.addActor(pipeline.tubeActorYZ);
        renderer.resetCamera();
        renderWindow.render();

        // const camera = renderer.getActiveCamera();
        // camera.elevation(30.)
        // camera.azimuth(30.)

        // setInterval(function () {
        //     pipeline.tubeActorXY.rotateX(2.)
        //     pipeline.tubeActorXZ.rotateZ(3.)
        //     pipeline.tubeActorYZ.rotateY(5.)
        //     renderWindow.render();
        // }, 100)

        window.pipeline = this.pipeline;

        renderWindow.render();
    }

    componentDidMount() {
        this.fullScreenRenderer = vtkFullScreenRenderWindow.newInstance({
            background: [0.0, 0.0, 0.0],
            rootContainer: this.container.current,
            containerStyle: {},
        });
        this.updatePipeline();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.resolution !== this.props.resolution) {
            this.updatePipeline();
        }
    }


    render() {
        return <div ref={this.container} />
    }
}
export default Custom;
