import React from 'react'

import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkFullScreenRenderWindow from 'vtk.js/Sources/Rendering/Misc/FullScreenRenderWindow';
import vtkSTLReader from 'vtk.js/Sources/IO/Geometry/STLReader';



class Stl extends React.Component {

    constructor(props) {
        super(props)
        this.fullScreenRenderer = null;
        this.container = React.createRef();
        this.pipeline = null;
    }

    createPipeline(resolution = 20) {

        const reader = vtkSTLReader.newInstance();
        if (this.props.stlFile){
            reader.parseAsArrayBuffer(this.props.stlFile);
            //reader.parseAsText(this.props.stlFile);
        }
    
        const mapper = vtkMapper.newInstance({ scalarVisibility: false });
        const objectActor = vtkActor.newInstance();
        
        objectActor.setMapper(mapper);
        mapper.setInputConnection(reader.getOutputPort());


        return { objectActor };
    }

    updatePipeline() {
        const renderer = this.fullScreenRenderer.getRenderer();
        const renderWindow = this.fullScreenRenderer.getRenderWindow();


        if (this.pipeline) {
            renderer.removeActor(this.pipeline.actor);
            this.pipeline = null;
        }

        const resolution = this.props.resolution || 40
        this.pipeline = this.createPipeline(resolution);
        const pipeline = this.pipeline
        renderer.addActor(pipeline.objectActor);
        renderer.resetCamera();
        renderWindow.render();

        // const camera = renderer.getActiveCamera();
        // camera.elevation(30.)
        // camera.azimuth(30.)

        // setInterval(function () {
        //     pipeline.tubeActorXY.rotateX(2.)
        //     pipeline.tubeActorXZ.rotateZ(3.)
        //     pipeline.tubeActorYZ.rotateY(5.)
        //     renderWindow.render();
        // }, 100)

        // window.pipeline = this.pipeline;

        renderWindow.render();
    }

    componentDidMount() {
        this.fullScreenRenderer = vtkFullScreenRenderWindow.newInstance({
            background: [0.0, 0.0, 0.0],
            rootContainer: this.container.current,
            containerStyle: {},
        });
        this.updatePipeline();
    }

    componentDidUpdate(props) {
        console.log(this.props.extraProp);
       // if (props.resolution !== this.props.resolution) {
            this.updatePipeline();
       // }
    }


    render() {
        return (<div ref={this.container} />)
    }
}
export default Stl;
