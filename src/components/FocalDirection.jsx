import React, { Component } from 'react';

class FocalDirection extends Component {

    

    constructor(props) {
        super(props);
        this.state = {
            cameraFocus: {x:0,y:0,z:0},
            step: this.props.step? this.props.step : 0.1
        }
    }

    _updateCameraFocalDirection = (deltax,deltay,deltaz) => {
        let x = this.state.cameraFocus.x + deltax
        let y = this.state.cameraFocus.y + deltay
        let z = this.state.cameraFocus.z + deltaz
        let coordinates={x:x,y:y,z:z}
        this.setState({
            cameraFocus:coordinates})
        this.props.changeFocalDirection(coordinates)
    }


    _handleClickUp = () => {
        this._updateCameraFocalDirection(this.state.step,0,0)
    }
    _handleClickDown = () => {
        this._updateCameraFocalDirection(-this.state.step,0,0)
    }
    _handleClickLeft = () => {
        this._updateCameraFocalDirection(0,-this.state.step,0)
    }
    _handleClickRight = () => {
        this._updateCameraFocalDirection(0,this.state.step,0)
    }
    _handleClickCenter = () => {
        let coordinates={x:0,y:0,z:0}
        this.setState({
            cameraFocus:coordinates})
        this.props.changeFocalDirection(coordinates)    
    }

    componentDidUpdate() {       
        //this.props.changeFocalDirection(this.state.cameraFocus)
    }

   

    render(){
        return (<div>
            <input type="button" value="Left" onClick={this._handleClickUp}/>
            <input type="button" value="Right" onClick={this._handleClickDown}/>
            <input type="button" value="Up" onClick={this._handleClickLeft}/>
            <input type="button" value="Down" onClick={this._handleClickRight}/>
            <input type="button" value="Center" onClick={this._handleClickCenter}/>
        </div>)
    }
  

}
export default FocalDirection;