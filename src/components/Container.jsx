import React, { Component } from 'react';

import View from './View'

import vtkSphereSource from 'vtk.js/Sources/Filters/Sources/SphereSource';
import vtkConeSource from 'vtk.js/Sources/Filters/Sources/ConeSource';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';

import FocalDirection from './FocalDirection';

class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modelsToShow: [],
            modelsToHide: [],
            cone: null,
            sphere: null,
            coneBoxIsChecked: false,
            sphereBoxIsChecked: false,
            isMouseEnable: true,
            cameraFocus: { x: 0, y: 0, z: 0 },
            conePosition: { x: 0, y: 0, z: 0 }
        }
    }

    _updateCameraFocalDirection = (coordinates) => {
        console.log(coordinates);
        this.setState({
            cameraFocus: coordinates
        })
    }

    _createCone = () => {
        if (!this.state.cone) {
            const coneSource = vtkConeSource.newInstance({ height: 5 })
            const actor = vtkActor.newInstance()
            const mapper = vtkMapper.newInstance()
            actor.setMapper(mapper)
            mapper.setInputConnection(coneSource.getOutputPort())
            return actor
        }
        return this.cone
    }


    _createSphere = () => {
        if (!this.state.sphere) {
            const sphereSource = vtkSphereSource.newInstance({ radius: 1 });
            const actor = vtkActor.newInstance()
            const mapper = vtkMapper.newInstance()
            actor.setMapper(mapper)
            mapper.setInputConnection(sphereSource.getOutputPort())
            return actor
        }
        return this.sphere
    }

    _handleMouseEvents = () => {
        this.setState({
            isMouseEnable: !this.state.isMouseEnable
        })
    }


    _handleChangeCone = () => {
        if (this.state.coneBoxIsChecked) {
            let modelsToShow = this.state.modelsToShow.filter(model => model !== this.state.cone)
            let modelsToHide = this.state.modelsToHide
            modelsToHide.push(this.state.cone)
            this._updateModelsInView(modelsToShow, this.state.modelsToHide, 'coneBoxIsChecked', !this.state.coneBoxIsChecked)

        } else {
            let modelsToHide = this.state.modelsToHide.filter(model => model !== this.state.cone)
            let modelsToShow = this.state.modelsToShow
            modelsToShow.push(this.state.cone)
            this._updateModelsInView(this.state.modelsToShow, modelsToHide, 'coneBoxIsChecked', !this.state.coneBoxIsChecked)
        }
        
    }

    _handleChangeSphere = () => {
        if (this.state.sphereBoxIsChecked) {
            let modelsToShow = this.state.modelsToShow.filter(model => model !== this.state.sphere)
            let modelsToHide = this.state.modelsToHide
            modelsToHide.push(this.state.sphere)
            this._updateModelsInView(modelsToShow, this.state.modelsToHide, 'sphereBoxIsChecked', !this.state.sphereBoxIsChecked)
        } else {
            let modelsToHide = this.state.modelsToHide.filter(model => model !== this.state.sphere)
            let modelsToShow = this.state.modelsToShow
            modelsToShow.push(this.state.sphere)
            this._updateModelsInView(this.state.modelsToShow, modelsToHide, 'sphereBoxIsChecked', !this.state.sphereBoxIsChecked)
        }
    }

    _updateModelsInView = (modelsToShow, modelsToHide, checkBoxKey, checkBoxValue) => {
        this.setState({
            modelsToHide: modelsToHide,
            modelsToShow: modelsToShow,
            [checkBoxKey]: checkBoxValue,
        })
        console.log(this.state.cone)
        console.log(this.state.cone.getVisibility())
        console.log(this.state.cone.getPosition())

    }

    componentDidMount() {
        this.setState({
            cone: this._createCone(),
            sphere: this._createSphere()
        })        
    }

    _handleClickCenter = () => {
        let positionnew = this.state.conePosition
        positionnew.x = positionnew.x + 1 
        this.setState({
            conePosition: positionnew
        })
        this.state.cone.setPosition(positionnew.x,positionnew.y, positionnew.z)
        
    }

    render() {
        return (
            <div >
                {/* <input type="checkbox" defaultChecked={this.state.isMouseEnable} onChange={this._handleMouseEvents} />Enable/Disable Mouse  */}
                <input type="checkbox" defaultChecked={this.state.conecbox} onChange={this._handleChangeCone} />Cone
                 <div style={{ pointerEvents: this.state.isMouseEnable ? 'auto' : 'none' }}>
                    <View modelsToShow={this.state.modelsToShow} modelsToHide={this.state.modelsToHide} cameraFocus={this.state.cameraFocus}></View>
                </div>
                {/* <input type="checkbox" defaultChecked={this.state.spherecbox} onChange={this._handleChangeSphere} />Sphere */}
                 {/* <FocalDirection changeFocalDirection={this._updateCameraFocalDirection}></FocalDirection> */}
                {/* <input type="button" value="Center" onClick={this._handleClickCenter}/> */}
            </div>
        );
    }
}

export default Container;



