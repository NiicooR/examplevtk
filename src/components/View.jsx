import React from 'react'

import vtkFullScreenRenderWindow from 'vtk.js/Sources/Rendering/Misc/FullScreenRenderWindow';



class View extends React.Component {

    constructor(props) {
        super(props)
        this.container = React.createRef();
        this.state= {
            renderer: null,
            renderWindow: null,
            fullScreenRenderer: null,

        }
        
    }
    componentDidUpdate() {       
        this.props.changeFocalDirection(this.state.cameraFocus)
    }

    updateCamera = (coordinates) => {
        let camera = this.state.camera
        let x = coordinates.x
        let y = coordinates.y
        let z = coordinates.z
        camera.setFocalPoint(x,y,z)
        this.state.renderWindow.render()
    }
    
    _moveCamera = (angle = 1, azimuth = 1) => {
        let camera = this.state.camera
        // this.state.camera.elevation(angle)
        // this.state.camera.azimuth(angle)
        // this.state.camera.yaw(angle)
        // this.state.camera.setPosition(5,0,15)//izq/derecha,arriba/abajo,cerca/lejosdelorigen
        camera.setPosition(0,0,15)

        // camera.setDistance(1) //setDistanceSteps in ZoomIn/ZoomOut
        //console.log(camera.getDistance());
        //console.log(camera.getDirectionOfProjection());
        camera.roll(0)//roll of camera 0-180 complete roll
        camera.zoom(1)//zoom. set default 1
        //console.log(camera.getFocalPoint());
        
        //camera.setFocalPoint(1,3,0) //izq/derecha,arriba/abajo,cerca/lejosdelorigen
        

    }
    // _moveCameraAzimuth = (angle = 1) => {
    //     this.state.camera.elevation(angle)
    // }
    updatePipeline(actor = [], deleteActor = []) {
        const renderer = this.state.renderer
        const renderWindow = this.state.renderWindow

        if(actor.length !== 0){
            actor.forEach(actor => {
                renderer.addActor(actor);
            });
        }
       
        if(deleteActor.length !== 0){
            deleteActor.forEach(actor => {
                renderer.removeActor(actor);
            });
        }
        // renderer.resetCamera(); 
        this._moveCamera(5)
        renderWindow.render();
    }

    componentDidMount() {
        let fullScreenRenderer = vtkFullScreenRenderWindow.newInstance({
            background: [0.0, 0.0, 0.0],
            rootContainer: this.container.current,
            containerStyle: {},
        });
        this.setState({
            fullScreenRenderer: fullScreenRenderer,
            renderer:fullScreenRenderer.getRenderer(),
            renderWindow:fullScreenRenderer.getRenderWindow(),
            camera: fullScreenRenderer.getRenderer().getActiveCamera()
        })
    }

    componentDidUpdate() {       
        this.updatePipeline(this.props.modelsToShow, this.props.modelsToHide)
        this.updateCamera(this.props.cameraFocus)
    }

    render() {
        return (<div ref={this.container} />)
    }
}
export default View;
